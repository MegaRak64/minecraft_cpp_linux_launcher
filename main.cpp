#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <curl/curl.h>
#include <stdio.h>
#include <vector>
#include <sys/stat.h>
#include <unistd.h>
#include "json.hpp"
#include "zip.h"

using json = nlohmann::json;
using namespace std;
/* Global variables */
vector<string> assemblies;
int choosen;
/* Global variables */

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

static size_t WriteFile(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

void curl_request_string(string url, string *output)
{
	CURL *curl = curl_easy_init();
	CURLcode res;
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback); 
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, output); 
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}
}

void get_assemblies()
{
	string asm_response;
	curl_request_string("http://megarak64.hldns.ru/mineupdater/assemblys_test.php", &asm_response);
	while (!asm_response.empty()) {
		string asm_response1 = asm_response.substr(0, asm_response.find(",", 0));
		assemblies.push_back(asm_response1);
		asm_response.erase(0, asm_response.find(",", 0) + 1);
	}
}

void curl_download_file(string url, string file, string path = "./")
{
	CURL *curl = curl_easy_init();
	CURLcode res;
	if (curl) {
		FILE *fp;
		fp = fopen((path + file).c_str(), "wb");
		curl_easy_setopt(curl, CURLOPT_URL, (url + file).c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteFile); 
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp); 
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		fclose(fp);
	}
	url = "";
}

int main()
{
	int mode;
	string nickname;
	get_assemblies();
	// goto
	START:
	for (int i = 0; i < assemblies.size(); i++)
	{
		cout << "[" << i << "] " << assemblies[i] << endl;
	}
	
	cout << "Write number of assemble: ";
	cin >> choosen;
	
	cout << endl << "[0] play" << endl << "[1] download" << endl << "Choose action: ";
	cin >> mode;
	
	mkdir("data/", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(("data/" + assemblies[choosen]).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	chdir(("data/" + assemblies[choosen]).c_str());
	mkdir("mods", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir("config", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);	
	
	switch (mode) {
	case (1):
		static json mods_json;
		static string mods_temp;
		curl_request_string("http://megarak64.hldns.ru/mineupdater/modconf.php?assembly=" + assemblies[choosen], &mods_temp);
	
		mods_json = json::parse(mods_temp);
	
		cout << endl;
		// download mods
		cout << "Downloading mods..." << endl;
		for (int i = 0; i < mods_json.at("mods").size(); i++)
		{
			cout << "Progress: " << i * 100 / mods_json.at("mods").size() << "%" << endl;
			if (mods_json["mods"][i].get<string>() == "ic2" || mods_json["mods"][i].get<string>() == "railcraft" || mods_json["mods"][i].get<string>() == "1.7.10")
				continue;
			curl_download_file("http://megarak64.hldns.ru/mineupdater/assemblys/"+ assemblies[choosen] +"/mods/", mods_json["mods"][i].get<string>(), "./mods/");
		}
		cout << "Downloading mods completed" << endl << "Downloading client...";
		// download client
		curl_download_file("http://megarak64.hldns.ru/mineupdater/", "1.7.10.zip");
		cout << "Downloading client completed" << endl;
	
		zip_extract("1.7.10.zip", "./", 0, nullptr);
		mods_json = nullptr;
		mods_temp = "";
		
		chdir("../../");
		
		goto START;
		//return 0;
	case (0):
		cout << endl << "Write your nickname: ";
		cin >> nickname;
		system(("java -Xmx2048M -XX:+AggressiveOpts -XX:+UseCompressedOops -Djava.library.path=natives -cp libraries/net/minecraftforge/forge/1.7.10-10.13.4.1614-1.7.10/forge-1.7.10-10.13.4.1614-1.7.10.jar:libraries/net/minecraft/launchwrapper/1.12/launchwrapper-1.12.jar:libraries/org/ow2/asm/asm-all/5.0.3/asm-all-5.0.3.jar:libraries/com/typesafe/akka/akka-actor_2.11/2.3.3/akka-actor_2.11-2.3.3.jar:libraries/com/typesafe/config/1.2.1/config-1.2.1.jar:libraries/org/scala-lang/scala-actors-migration_2.11/1.1.0/scala-actors-migration_2.11-1.1.0.jar:libraries/org/scala-lang/scala-compiler/2.11.1/scala-compiler-2.11.1.jar:libraries/org/scala-lang/plugins/scala-continuations-library_2.11/1.0.2/scala-continuations-library_2.11-1.0.2.jar:libraries/org/scala-lang/plugins/scala-continuations-plugin_2.11.1/1.0.2/scala-continuations-plugin_2.11.1-1.0.2.jar:libraries/org/scala-lang/scala-library/2.11.1/scala-library-2.11.1.jar:libraries/org/scala-lang/scala-parser-combinators_2.11/1.0.1/scala-parser-combinators_2.11-1.0.1.jar:libraries/org/scala-lang/scala-reflect/2.11.1/scala-reflect-2.11.1.jar:libraries/org/scala-lang/scala-swing_2.11/1.0.1/scala-swing_2.11-1.0.1.jar:libraries/org/scala-lang/scala-xml_2.11/1.0.2/scala-xml_2.11-1.0.2.jar:libraries/lzma/lzma/0.0.1/lzma-0.0.1.jar:libraries/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar:libraries/com/google/guava/guava/17.0/guava-17.0.jar:libraries/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.jar:libraries/com/mojang/realms/1.3.5/realms-1.3.5.jar:libraries/org/apache/commons/commons-compress/1.8.1/commons-compress-1.8.1.jar:libraries/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar:libraries/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar:libraries/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar:libraries/java3d/vecmath/1.3.1/vecmath-1.3.1.jar:libraries/net/sf/trove4j/trove4j/3.0.3/trove4j-3.0.3.jar:libraries/com/ibm/icu/icu4j-core-mojang/51.2/icu4j-core-mojang-51.2.jar:libraries/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar:libraries/com/paulscode/codecjorbis/20101023/codecjorbis-20101023.jar:libraries/com/paulscode/codecwav/20101023/codecwav-20101023.jar:libraries/com/paulscode/libraryjavasound/20101123/libraryjavasound-20101123.jar:libraries/com/paulscode/librarylwjglopenal/20100824/librarylwjglopenal-20100824.jar:libraries/com/paulscode/soundsystem/20120107/soundsystem-20120107.jar:libraries/io/netty/netty-all/4.0.10.Final/netty-all-4.0.10.Final.jar:libraries/com/google/guava/guava/15.0/guava-15.0.jar:libraries/org/apache/commons/commons-lang3/3.1/commons-lang3-3.1.jar:libraries/commons-io/commons-io/2.4/commons-io-2.4.jar:libraries/commons-codec/commons-codec/1.9/commons-codec-1.9.jar:libraries/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar:libraries/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar:libraries/com/google/code/gson/gson/2.2.4/gson-2.2.4.jar:libraries/com/mojang/authlib/1.5.21/authlib-1.5.21.jar:libraries/org/apache/logging/log4j/log4j-api/2.0-beta9/log4j-api-2.0-beta9.jar:libraries/org/apache/logging/log4j/log4j-core/2.0-beta9/log4j-core-2.0-beta9.jar:libraries/org/lwjgl/lwjgl/lwjgl/2.9.1/lwjgl-2.9.1.jar:libraries/org/lwjgl/lwjgl/lwjgl_util/2.9.1/lwjgl_util-2.9.1.jar:libraries/tv/twitch/twitch/5.16/twitch-5.16.jar:versions/1.7.10/1.7.10.jar net.minecraft.launchwrapper.Launch --username " + nickname + "--version 1.7.10-Forge10.13.4.1614-1.7.10 --gameDir ./ --assetsDir assets --assetIndex 1.7.10 --uuid 0 --accessToken 0 --userProperties {} --userType legacy --tweakClass cpw.mods.fml.common.launcher.FMLTweaker").c_str());
		exit(0);
	default:
		break;
	}
	
    return 0;
}
